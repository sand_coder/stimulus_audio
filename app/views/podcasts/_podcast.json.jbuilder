json.extract! podcast, :id, :title, :duration, :created_at, :updated_at
json.url podcast_url(podcast, format: :json)
